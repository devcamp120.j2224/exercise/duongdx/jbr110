public class Circle {
    private double radius = 1.0d;

    // Constructor
    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    // Getter & Setter
    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    // method
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double getCircumference() {
        return Math.PI * radius * 2;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }
}
