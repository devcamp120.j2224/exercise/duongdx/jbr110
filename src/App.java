public class App {
    public static void main(String[] args) throws Exception {
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);
        System.out.println("circle1: " + circle1.toString());
        System.out.println("circle1's area: " + circle1.getArea());
        System.out.println("circle1's circumference: " + circle1.getCircumference());
        System.out.println();
        System.out.println("circle2: " + circle2.toString());
        System.out.println("circle2's area: " + circle2.getArea());
        System.out.println("circle2's circumference: " + circle2.getCircumference());
    }
}
